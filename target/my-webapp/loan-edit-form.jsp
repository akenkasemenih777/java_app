<%@ page import="ru.darenina.model.Loan" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<html>
<%--protected int id;
protected int client_id;
protected Date date_of_receipt;
protected Date last_payment_date;
protected Date payment_term_date;
protected float annual_interest;
protected float debt_balance;
protected float total_loan_amount;--%>
<head>
    <title>User Management Application</title>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
</head>
<body>
<div class="container">
    <h3 class="text-center">Форма редактирования кредита</h3>
    <hr>
<form method="post" action="<%=request.getContextPath()%>/updateloan?id=<%out.print(((Loan)request.getAttribute("loan")).getIdLoan());%>">
    <div>
    <label>Клиент:
        <input type="text" name="client_id" value=<%out.print(((Loan)request.getAttribute("loan")).getClientId());%>><br/>
    </label>
    </div>
    <div>
    <label>Дата получения кредита:
        <input type="text" name="date_of_receipt" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" value=<%out.print(((Loan)request.getAttribute("loan")).getDateOfReceipt());%>><br />
    </label>
    </div>
    <div>
    <label>Дата последнего платежа:
        <input type="text" name="last_payment_date" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" value=<%out.print(((Loan)request.getAttribute("loan")).getLastPaymentDate());%>><br />
    </label>
    </div>
    <div>
    <label>Срок выплаты кредита:
        <input type="text" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" name="payment_term_date" value=<%out.print(((Loan)request.getAttribute("loan")).getPaymentTermDate());%>><br />
    </label>
    </div>
    <div>
    <label>Годовой процент:
        <input type="text" name="annual_interest" value=<%out.print(((Loan)request.getAttribute("loan")).getAnnualInterest());%>><br />
    </label>
    </div>
    <div>
    <label>Осталось выплатить:
        <input type="text" name="debt_balance" value=<%out.print(((Loan)request.getAttribute("loan")).getDebtBalance());%>><br />
    </label>
    </div>
    <div>
    <label>Общая сумма кредита:
        <input type="text" name="total_loan_amount" value=<%out.print(((Loan)request.getAttribute("loan")).getTotalLoanAmount());%>><br />
    </label>
    </div>
    <div>
    <button type=submit>Сохранить</button>
    </div>
</form>
    <a href="<%=request.getContextPath()%>/loanslist" >Отменить</a>
</div>
</body>
</html>
