<%@ page import="ru.darenina.model.Clients" %>
<%@ page import="java.util.List" %>
<%@ page import="ru.darenina.model.Clients" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<html>
<head>
    <title>User Management Application</title>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
</head>
<body>
<div class="row">
    <div class="container" align="left">
        <p>

            <a href="<%=request.getContextPath()%>/list" class="btn btn-success">Сотрудники</a>
            <a href="<%=request.getContextPath()%>/clientslist" class="btn btn-success">Клиенты</a>
            <a href="<%=request.getContextPath()%>/loanslist" class="btn btn-success">Кредиты</a>
            <a href="<%=request.getContextPath()%>/debtlist" class="btn btn-success">Должники</a>

        </p>
    </div>

    <div class="container">
        <h3 class="text-center">Должники</h3>
        <hr>
        <br>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Фамилия</th>
                <th>Имя</th>
                <th>Отчество</th>
            </tr>
            </thead>
            <tbody>
            <%
                List<Clients> clients = (List<Clients>) request.getAttribute("listClient");

                if (clients != null && !clients.isEmpty()) {
                    for (Clients c : clients) {
                        out.println("<tr>");
                        out.println("<td>" + c.getSurnameClient() + "</td>");
                        out.println("<td>" + c.getNameClient() + "</td>");
                        out.println("<td>" + c.getPatronymicClient() + "</td>");
                        out.println("</tr>");
                    }
                } else out.println("<p>There are no clients yet!</p>");
            %>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>
