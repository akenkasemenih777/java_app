<%@ page import="ru.darenina.model.User" %>
<%@ page import="java.util.List" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<html>
<head>
<title>User Management Application</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
	<div class="row">

		<div class="container" align="left">
			<p>

				<a href="<%=request.getContextPath()%>/list" class="btn btn-success">Сотрудники</a>
			<a href="<%=request.getContextPath()%>/clientslist" class="btn btn-success">Клиенты</a>
					<a href="<%=request.getContextPath()%>/loanslist" class="btn btn-success">Кредиты</a>
				<a href="<%=request.getContextPath()%>/debtlist" class="btn btn-success">Должники</a>
			</p>
		</div>


		<div class="container">
			<h3 class="text-center">Сотрудники</h3>
			<hr>
			<div class="w3-btn w3-hover-light-blue w3-round-large">

				<a href="<%=request.getContextPath()%>/new" class="btn btn-success">Добавить нового сотрудника</a>
			</div>
			<br>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Фамилия</th>
						<th>Имя</th>
						<th>Отчетство</th>
						<th>Действия</th>
					</tr>
				</thead>
				<tbody>
				<%
					List<User> users = (List<User>) request.getAttribute("listUser");

					if (users != null && !users.isEmpty()) {
						for (User u : users) {
							out.println("<tr>");
							out.println("<td>" + u.getSurname() + "</td>");
							out.println("<td>" + u.getName() + "</td>");
							out.println("<td>" + u.getPatronymic() + "</td>");
							out.println("<td><a href=" + request.getContextPath() + "/edit?id=" + u.getId() + ">Редактировать</a>");
							out.print("<a href=" + request.getContextPath() + "/delete?id=" + u.getId() + ">Удалить</a></td>");
							out.println("</tr>");
						}
					} else out.println("<p>There are no users yet!</p>");
				%>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>
