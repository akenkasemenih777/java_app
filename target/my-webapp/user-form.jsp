<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
<title>User Management Application</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>

<div class="row">

<div class="container">
	<h3 class="text-center">Форма создания пользователя</h3>
	<hr>
<form method="post" action="<%=request.getContextPath()%>/insert">
	<div>
	<label>Фамилия:
		<input type="text" name="surname"><br />
	</label>
	<label>Имя:
		<input type="text" name="name"><br />
	</label>
	<label>Отчетство:
		<input type="text" name="patronymic"><br />
	</label>
	</div>
	<hr>
	<label>Роль:
		<input type="text" name="role"><br />
	</label>
	<label>Филиал:
		<input type="text" name="branch"><br />
	</label>
	<hr>
	<div>
	<label>Логин:
		<input type="text" name="login"><br />
	</label>
	<label>Пароль:
		<input type="text" name="password"><br />
	</label>
	</div>
	<hr>
	<div><button type=submit>Сохранить</button></div>
</form>
	<a href="<%=request.getContextPath()%>/list" >Отменить</a>
</div>
</div>
</body>
</html>
