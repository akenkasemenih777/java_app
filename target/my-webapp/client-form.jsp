<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
<title>User Management Application</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
<div class="container">
	<h3 class="text-center">Форма создания клиента</h3>
	<hr>
<form method="post" action="<%=request.getContextPath()%>/insertclient">
	<div>
	<label>Фамилия:
		<input type="text" name="surname"><br />
	</label>

	<label>Имя:
		<input type="text" name="name"><br />
	</label>


	<label>Отчество:
		<input type="text" name="patronymic"><br />
	</label>
	</div>
	<label>Закреплённый сотрудник:
		<input type="text" name="employee_id"><br />
	</label>
	<div>
	<button type=submit>Сохранить</button>
	</div>
</form>
	<a href="<%=request.getContextPath()%>/clientslist" >Отменить</a>
</div>
</body>
</html>
