<%@ page import="ru.darenina.model.Clients" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<html>
<head>
    <title>User Management Application</title>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
</head>
<body>
<div class="container">
    <h3 class="text-center">Форма редактирования клиента</h3>
    <hr>
<form method="post" action="<%=request.getContextPath()%>/updateclient?id=<%out.print(((Clients)request.getAttribute("client")).getIdClient());%>">
    <div>
    <label>Имя:
        <input type="text" name="name" value=<%out.print(((Clients)request.getAttribute("client")).getNameClient());%>><br/>
    </label>

    <label>Фамилия:
        <input type="text" name="surname" value=<%out.print(((Clients)request.getAttribute("client")).getSurnameClient());%>><br />
    </label>

    <label>Отчество:
        <input type="text" name="patronymic" value=<%out.print(((Clients)request.getAttribute("client")).getPatronymicClient());%>><br />
    </label>
    </div>
    <div>
    <label>Ответственный сотрудник:
        <input type="text" name="employee_id" value=<%out.print(((Clients)request.getAttribute("client")).getEmployeeClient());%>><br />
    </label>
    </div>
    <div>
    <button type=submit>Сохранить</button>
    </div>
</form>
    <a href="<%=request.getContextPath()%>/clientslist" >Отменить</a>
</div>
</body>
</html>
