<%@ page import="ru.darenina.model.User" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<html>
<head>
    <title>User Management Application</title>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
</head>
<body>
<div class="row">

    <div class="container">
        <h3 class="text-center">Форма редактирования пользователя</h3>
        <hr>
<form method="post" action="<%=request.getContextPath()%>/update?id=<%out.print(((User)request.getAttribute("user")).getId());%>">
    <div>
        <label>Фамилия:
            <input type="text" name="surname" value=<%out.print(((User)request.getAttribute("user")).getSurname());%>><br />
        </label>
    <label>Имя:
        <input type="text" name="name" value=<%out.print(((User)request.getAttribute("user")).getName());%>><br/>
    </label>
    <label>Отчество:
        <input type="text" name="patronymic" value=<%out.print(((User)request.getAttribute("user")).getPatronymic());%>><br />
    </label>
        </div>
    <hr>
    <div>
    <label>Роль:
        <input type="text" name="role" value=<%out.print(((User)request.getAttribute("user")).getRole());%>><br />
    </label>
    <label>Филиал:
        <input type="text" name="branch" value=<%out.print(((User)request.getAttribute("user")).getBranch());%>><br />
    </label>
    </div>
    <hr>
    <div>
    <label>Логин:
        <input type="text" name="login" value=<%out.print(((User)request.getAttribute("user")).getLogin());%>><br />
    </label>
    <label>Пароль:
        <input type="text" name="password" value=<%out.print(((User)request.getAttribute("user")).getPassword());%>><br />
    </label>
    </div>
    <hr>
    <button type=submit>Сохранить</button>
</form>
        <a href="<%=request.getContextPath()%>/list" >Отменить</a>
    </div>
</div>
</body>
</html>
