package ru.darenina.model;

public class Clients {
    protected int id;
    protected String name;
    protected String surname;
    protected String patronymic;
    protected int employee;


    public Clients() {
    }

    public Clients(String name, String surname, String patronymic) {
        super();
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
    }

    public Clients(int id, String name, String surname, String patronymic, int employee) {
        super();
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.employee = employee;

    }
    public Clients(String name, String surname, String patronymic, int employee) {
        super();
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.employee = employee;

    }

    public int getIdClient() {
        return id;
    }
    public String getNameClient() {
        return name;
    }
    public String getSurnameClient() {
        return surname;
    }
    public String getPatronymicClient() {
        return patronymic;
    }
    public int getEmployeeClient() {
        return employee;
    }

}
