package ru.darenina.model;

public class User {
	protected int id;
	protected String name;
	protected String surname;
	protected String patronymic;
	protected int role;
	protected int branch;
	protected String login;
	protected String password;
	
	public User() {
	}
	
	public User(String name, String surname, String patronymic) {
		super();
		this.name = name;
		this.surname = surname;
		this.patronymic = patronymic;
	}

	public User(int id, String name, String surname, String patronymic, int role, int branch, String login, String password) {
		super();
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.patronymic = patronymic;
		this.role = role;
		this.branch = branch;
		this.login = login;
		this.password = password;

	}
	public User(String name, String surname, String patronymic, int role, int branch, String login, String password) {
		super();
		this.name = name;
		this.surname = surname;
		this.patronymic = patronymic;
		this.role = role;
		this.branch = branch;
		this.login = login;
		this.password = password;

	}

	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getSurname() {
		return surname;
	}
	public String getPatronymic() {
		return patronymic;
	}

	public int getRole() {
		return role;
	}

	public int getBranch() {
		return branch;
	}

	public String getLogin() {
		return login;
	}

	public String getPassword() {
		return password;
	}
}
