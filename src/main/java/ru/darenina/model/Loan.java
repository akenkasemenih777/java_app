package ru.darenina.model;

import java.util.Date;

public class Loan {

    protected int id;
    protected int client_id;
    protected Date date_of_receipt;
    protected Date last_payment_date;
    protected Date payment_term_date;
    protected float annual_interest;
    protected float debt_balance;
    protected float total_loan_amount;

    public Loan() {
    }

    public Loan(Date date_of_receipt, float total_loan_amount) {
        super();
        this.date_of_receipt = date_of_receipt;
        this.total_loan_amount = total_loan_amount;
    }

    public Loan(int id, int client_id, Date date_of_receipt, Date last_payment_date,
                Date payment_term_date, float annual_interest,float debt_balance, float total_loan_amount) {
        super();
        this.id = id;
        this.client_id = client_id;
        this.date_of_receipt = date_of_receipt;
        this.last_payment_date = last_payment_date;
        this.payment_term_date = payment_term_date;
        this.annual_interest = annual_interest;
        this.debt_balance = debt_balance;
        this.total_loan_amount = total_loan_amount;

    }
    public Loan(int client_id, Date date_of_receipt, Date last_payment_date,
                Date payment_term_date, float annual_interest,float debt_balance, float total_loan_amount) {
        super();
        this.client_id = client_id;
        this.date_of_receipt = date_of_receipt;
        this.last_payment_date = last_payment_date;
        this.payment_term_date = payment_term_date;
        this.annual_interest = annual_interest;
        this.debt_balance = debt_balance;
        this.total_loan_amount = total_loan_amount;

    }

    public int getIdLoan() {
        return id;
    }
    public Date getDateOfReceipt() {
        return date_of_receipt;
    }
    public Date getLastPaymentDate() {
        return last_payment_date;
    }
    public Date getPaymentTermDate() {
        return payment_term_date;
    }

    public float getAnnualInterest() {
        return annual_interest;
    }

    public float getDebtBalance() {
        return debt_balance;
    }

    public float getTotalLoanAmount() {
        return total_loan_amount;
    }

    public int getClientId() {
        return client_id;
    }
}
