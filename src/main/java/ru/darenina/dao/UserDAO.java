package ru.darenina.dao;

import ru.darenina.model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class UserDAO {


	private static final String INSERT_USERS_SQL = "insert into Employees(employee_name, employee_surname, employee_patronymic, role_id, branch_id, emplogin, emppassword) values(?, ?, ?, ?, ?, ?, ?)";

	private static final String SELECT_USER_BY_ID = "select * from Employees where employee_id = ?";

	private static final String SELECT_ALL_USERS = "select * from Employees";

	private static final String DELETE_USERS_SQL = "delete from Employees where employee_id = ?";

	private static final String UPDATE_USERS_SQL = "update Employees set employee_name = ?, employee_surname= ?, employee_patronymic = ?, role_id = ?, branch_id = ?, emplogin = ?, emppassword = ? where employee_id = ?";

	private static final String SELECT_USER_BY_LOGIN = "select * from Employees where emplogin = ?, emppassword = ?";

	public UserDAO() {
	}

	protected Connection getConnection() {
		Connection connection = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String jdbcURL = "jdbc:sqlserver://localhost:1433;database=subd_db";
			String jdbcUsername = "sa";
			String jdbcPassword = "12345";
			connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}

		return connection;
	}

	public void insertUser(User user) {
		try (Connection connection = getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USERS_SQL)) {

			preparedStatement.setString(1, user.getName());
			preparedStatement.setString(2, user.getSurname());
			preparedStatement.setString(3, user.getPatronymic());
			preparedStatement.setInt(4, user.getRole());
			preparedStatement.setInt(5, user.getBranch());
			preparedStatement.setString(6, user.getLogin());
			preparedStatement.setString(7, user.getPassword());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			printSQLException(e);
		}
	}

	public User selectUser(int id) {
		User user = null;
		try (Connection connection = getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_BY_ID)) {

			preparedStatement.setInt(1, id);
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				String name = rs.getString("employee_name");
				String surname = rs.getString("employee_surname");
				String patronymic = rs.getString("employee_patronymic");
				int role = rs.getInt("role_id");
				int branch = rs.getInt("branch_id");
				String login = rs.getString("emplogin");
				String password = rs.getString("emppassword");
				user = new User(id, name, surname, patronymic, role, branch, login, password);
			}
		} catch (SQLException e) {
			printSQLException(e);
		}
		return user;
	}

	public User selectUserByLogin(String emplogin, String emppassword) {
		User user = null;
		try (Connection connection = getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_BY_LOGIN)) {

			preparedStatement.setString(1, emplogin);
			preparedStatement.setString(1, emppassword);
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				String name = rs.getString("employee_name");
				String surname = rs.getString("employee_surname");
				String patronymic = rs.getString("employee_patronymic");
				int role = rs.getInt("role_id");
				int branch = rs.getInt("branch_id");
				String login = rs.getString("emplogin");
				String password = rs.getString("emppassword");
				user = new User( name, surname, patronymic, role, branch, login, password);
			}
		} catch (SQLException e) {
			printSQLException(e);
		}
		return user;
	}

	public List<User> selectAllUsers() {
		List<User> users = new ArrayList<>();
		try (Connection connection = getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_USERS)) {

			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("employee_id");
				String name = rs.getString("employee_name");
				String surname = rs.getString("employee_surname");
				String patronymic = rs.getString("employee_patronymic");
				int role = rs.getInt("role_id");
				int branch = rs.getInt("branch_id");
				String login = rs.getString("emplogin");
				String password = rs.getString("emppassword");
				users.add(new User(id,name, surname, patronymic, role, branch, login, password));
			}
		} catch (SQLException e) {
			printSQLException(e);
		}
		return users;
	}

	public void deleteUser(int id) {
		try (Connection connection = getConnection();
				PreparedStatement statement = connection.prepareStatement(DELETE_USERS_SQL)) {

			statement.setInt(1, id);
			statement.executeUpdate();
		} catch (SQLException e) {
			printSQLException(e);
		}
	}

	public void updateUser(User user) {
		try (Connection connection = getConnection();
				PreparedStatement statement = connection.prepareStatement(UPDATE_USERS_SQL)) {

			statement.setString(1, user.getName());
			statement.setString(2, user.getSurname());
			statement.setString(3, user.getPatronymic());
			statement.setInt(4, user.getRole());
			statement.setInt(5, user.getBranch());
			statement.setString(6, user.getLogin());
			statement.setString(7, user.getPassword());
			statement.setInt(8, user.getId());
			statement.executeUpdate();
		} catch (SQLException e) {
			printSQLException(e);
		}
	}

	private void printSQLException(SQLException ex) {
		for (Throwable e : ex) {
			if (e instanceof SQLException) {
				e.printStackTrace(System.err);
				System.err.println("SQLState: " + ((SQLException) e).getSQLState());
				System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
				System.err.println("Message: " + e.getMessage());
				Throwable t = ex.getCause();
				while (t != null) {
					System.out.println("Cause: " + t);
					t = t.getCause();
				}
			}
		}
	}


}
