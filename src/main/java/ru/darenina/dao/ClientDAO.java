package ru.darenina.dao;

import ru.darenina.model.Clients;
import ru.darenina.model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class ClientDAO {

    private static final String INSERT_CLIENTS_SQL = "insert into Clients(client_name, client_surname, client_patronymic, employee_id) values(?, ?, ?, ?)";

    private static final String SELECT_CLIENT_BY_ID = "select * from Clients where client_id = ?";

    private static final String SELECT_ALL_CLIENTS = "select * from Clients";

    private static final String DELETE_CLIENT_SQL = "delete from Clients where client_id = ?";

    private static final String UPDATE_CLIENTS_SQL = "update Clients set client_name = ?, client_surname= ?, client_patronymic = ?, employee_id = ? where client_id = ?";

    private static final String SELECT_CLIENTS_BY_LOAN = "select client_surname, client_name, client_patronymic from Clients where client_id in (select client_id from Loans where DATEDIFF(D,last_payment_date, Convert(date, getdate()))>30)";

    public ClientDAO() {
    }

    protected Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String jdbcURL = "jdbc:sqlserver://localhost:1433;database=subd_db";
            String jdbcUsername = "sa";
            String jdbcPassword = "12345";
            connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return connection;
    }

    public void insertClient(Clients client) {
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_CLIENTS_SQL)) {

            preparedStatement.setString(1, client.getNameClient());
            preparedStatement.setString(2, client.getSurnameClient());
            preparedStatement.setString(3, client.getPatronymicClient());
            preparedStatement.setInt(4, client.getEmployeeClient());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    public Clients selectClient(int id) {
        Clients debclient = null;
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_CLIENT_BY_ID)) {

            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                String name = rs.getString("client_name");
                String surname = rs.getString("client_surname");
                String patronymic = rs.getString("client_patronymic");
                int employee_id = rs.getInt("employee_id");
                debclient = new Clients(id, name, surname, patronymic, employee_id);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return debclient;
    }

    public List<Clients> selectClientByLoans() {
        List<Clients> clients = new ArrayList<>();
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_CLIENTS_BY_LOAN)) {
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {

                String name = rs.getString("client_name");
                String surname = rs.getString("client_surname");
                String patronymic = rs.getString("client_patronymic");

                clients.add(new Clients(name, surname, patronymic));
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return clients;
    }

    public List<Clients> selectAllClient() {
        List<Clients> clients = new ArrayList<>();
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_CLIENTS)) {

            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("client_id");
                String name = rs.getString("client_name");
                String surname = rs.getString("client_surname");
                String patronymic = rs.getString("client_patronymic");
                int employee_id = rs.getInt("employee_id");
                clients.add(new Clients(id,name, surname, patronymic, employee_id));
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return clients;
    }

    public void deleteClient(int id) {
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_CLIENT_SQL)) {

            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    public void updateClient(Clients client) {
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_CLIENTS_SQL)) {

            statement.setString(1, client.getNameClient());
            statement.setString(2, client.getSurnameClient());
            statement.setString(3, client.getPatronymicClient());
            statement.setInt(4, client.getEmployeeClient());
            statement.setInt(5, client.getIdClient());
            statement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    private void printSQLException(SQLException ex) {
        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
}
