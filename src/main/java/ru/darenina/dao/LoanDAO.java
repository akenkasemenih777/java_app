package ru.darenina.dao;

import ru.darenina.model.Clients;
import ru.darenina.model.User;
import ru.darenina.model.Loan;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class LoanDAO {

    private static final String INSERT_LOAN_SQL = "insert into Loans(client_id, date_of_receipt, last_payment_date," +
            " payment_term_date, annual_interest, debt_balance, total_loan_amount) values(?, ?, ?, ?, ?, ?, ?)";

    private static final String SELECT_LOAN_BY_ID = "select * from Loans where loan_id = ?";

    private static final String SELECT_ALL_LOANS = "select * from Loans";

    private static final String DELETE_LOAN_SQL = "delete from Loans where loan_id = ?";

    private static final String UPDATE_LOANS_SQL = "update Loans set client_id = ?, date_of_receipt = ?, last_payment_date = ?," +
            " payment_term_date = ?, annual_interest = ?, debt_balance = ?, total_loan_amount = ? where loan_id = ?";

    public LoanDAO() {
    }

    protected Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String jdbcURL = "jdbc:sqlserver://localhost:1433;database=subd_db";
            String jdbcUsername = "sa";
            String jdbcPassword = "12345";
            connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return connection;
    }

    public void insertLoan(Loan loan) {
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_LOAN_SQL)) {

            preparedStatement.setInt(1, loan.getClientId());
            preparedStatement.setDate(2, toSqlDate(loan.getDateOfReceipt()));
            preparedStatement.setDate(3, toSqlDate(loan.getLastPaymentDate()));
            preparedStatement.setDate(4, toSqlDate(loan.getPaymentTermDate()));
            preparedStatement.setFloat(5,loan.getAnnualInterest());
            preparedStatement.setFloat(6,loan.getDebtBalance());
            preparedStatement.setFloat(7,loan.getTotalLoanAmount());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    public Loan selectLoan(int id) {
        Loan loan = null;
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_LOAN_BY_ID)) {

            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Date date_of_receipt = rs.getDate("date_of_receipt");
                Date last_payment_date = rs.getDate("last_payment_date");
                Date payment_term_date = rs.getDate("payment_term_date");
                float annual_interest = rs.getFloat("annual_interest");
                float debt_balance = rs.getFloat("debt_balance");
                float total_loan_amount = rs.getFloat("total_loan_amount");
                int client_id = rs.getInt("client_id");
                loan = new Loan(id,client_id, date_of_receipt, last_payment_date, payment_term_date, annual_interest, debt_balance, total_loan_amount);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return loan;
    }

    public List<Loan> selectAllLoans() {
        List<Loan> loan = new ArrayList<>();
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_LOANS)) {

            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int loan_id = rs.getInt("loan_id");
                Date date_of_receipt = rs.getDate("date_of_receipt");
                Date last_payment_date = rs.getDate("last_payment_date");
                Date payment_term_date = rs.getDate("payment_term_date");
                float annual_interest = rs.getFloat("annual_interest");
                float debt_balance = rs.getFloat("debt_balance");
                float total_loan_amount = rs.getFloat("total_loan_amount");
                int client_id = rs.getInt("client_id");
                loan.add(new Loan(loan_id,client_id, date_of_receipt, last_payment_date, payment_term_date, annual_interest, debt_balance, total_loan_amount));
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return loan;
    }

    public void deleteLoan(int id) {
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_LOAN_SQL)) {

            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    public void updateLoan(Loan loan) {
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_LOANS_SQL)) {
            statement.setInt(1, loan.getClientId());
            statement.setDate(2, toSqlDate(loan.getDateOfReceipt()));
            statement.setDate(3, toSqlDate(loan.getLastPaymentDate()));
            statement.setDate(4, toSqlDate(loan.getPaymentTermDate()));
            statement.setFloat(5,loan.getAnnualInterest());
            statement.setFloat(6,loan.getDebtBalance());
            statement.setFloat(7,loan.getTotalLoanAmount());
            statement.setInt(8, loan.getIdLoan());
            statement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    private Date toSqlDate(java.util.Date input) {
        if (input == null) {
            return null;
        }
        return new Date(input.getTime());
    }

    private void printSQLException(SQLException ex) {
        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
}
