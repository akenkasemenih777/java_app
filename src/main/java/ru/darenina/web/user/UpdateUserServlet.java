package ru.darenina.web.user;
import ru.darenina.dao.UserDAO;
import ru.darenina.model.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

@WebServlet("/update")
public class UpdateUserServlet  extends HttpServlet{

    private UserDAO userDAO;

    public void init() {
        userDAO = new UserDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, UnsupportedEncodingException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, UnsupportedEncodingException {
        request.setCharacterEncoding("UTF-8");
        try {

            updateUser(request, response);


        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }
    private void updateUser(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        int id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        String patronymic = request.getParameter("patronymic");
        int role = Integer.parseInt(request.getParameter("role"));
        int branch = Integer.parseInt(request.getParameter("branch"));
        String login = request.getParameter("login");
        String password = request.getParameter("password");


        User user = new User(id, name, surname, patronymic, role, branch, login, password);
        userDAO.updateUser(user);
        response.sendRedirect("list");
    }

}

