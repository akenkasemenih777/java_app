package ru.darenina.web.client;
import ru.darenina.dao.ClientDAO;
import ru.darenina.model.Clients;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

@WebServlet("/updateclient")
public class UpdateClientServlet extends HttpServlet{

    private ClientDAO clientDAO;

    public void init() {
        clientDAO = new ClientDAO();
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, UnsupportedEncodingException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, UnsupportedEncodingException {
        request.setCharacterEncoding("UTF-8");

        try {
            updateClient(request, response);


        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }
    private void updateClient(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        int id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        String patronymic = request.getParameter("patronymic");
        int employee_id = Integer.parseInt(request.getParameter("employee_id"));
        Clients client = new Clients(id, name, surname, patronymic, employee_id);
        clientDAO.updateClient(client);
        response.sendRedirect("clientslist");
    }

}

