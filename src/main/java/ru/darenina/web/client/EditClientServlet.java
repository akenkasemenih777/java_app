package ru.darenina.web.client;

import ru.darenina.dao.ClientDAO;
import ru.darenina.model.Clients;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

@WebServlet("/editclient")
public class EditClientServlet extends HttpServlet{

    private ClientDAO clientDAO;

    public void init() {
        clientDAO = new ClientDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, UnsupportedEncodingException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, UnsupportedEncodingException {
        request.setCharacterEncoding("UTF-8");
        try {
            showEditFormClient(request, response);


        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }

    private void showEditFormClient(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Clients existingClient = clientDAO.selectClient(id);
        RequestDispatcher dispatcher = request.getRequestDispatcher("client-edit-form.jsp");
        request.setAttribute("client", existingClient);
        dispatcher.forward(request, response);

    }

}

