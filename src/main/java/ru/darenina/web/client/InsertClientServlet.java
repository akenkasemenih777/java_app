package ru.darenina.web.client;

import ru.darenina.dao.ClientDAO;
import ru.darenina.model.Clients;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

@WebServlet("/insertclient")
public class InsertClientServlet extends HttpServlet{

    private ClientDAO clientDAO;

    public void init() {
        clientDAO = new ClientDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, UnsupportedEncodingException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, UnsupportedEncodingException {
        request.setCharacterEncoding("UTF-8");

        try {
            insertClient(request, response);


        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }
    private void insertClient(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        String patronymic = request.getParameter("patronymic");
        int employee_id = Integer.parseInt(request.getParameter("employee_id"));
        Clients newClient = new Clients(name, surname, patronymic, employee_id);
        clientDAO.insertClient(newClient);
        response.sendRedirect("clientslist");
    }
}
