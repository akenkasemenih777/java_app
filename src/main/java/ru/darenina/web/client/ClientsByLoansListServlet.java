package ru.darenina.web.client;

import ru.darenina.dao.ClientDAO;
import ru.darenina.model.Clients;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

@WebServlet("/debtlist")
public class ClientsByLoansListServlet extends HttpServlet {

    private ClientDAO clientDAO;

    public void init() {
        clientDAO = new ClientDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, UnsupportedEncodingException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, UnsupportedEncodingException {
        request.setCharacterEncoding("UTF-8");

        try {
            listClientByLoans(request, response);

        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }

    private void listClientByLoans(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        List<Clients> listClient = clientDAO.selectClientByLoans();
        request.setAttribute("listClient", listClient);
        RequestDispatcher dispatcher = request.getRequestDispatcher("debts-list.jsp");
        dispatcher.forward(request, response);
    }

}
