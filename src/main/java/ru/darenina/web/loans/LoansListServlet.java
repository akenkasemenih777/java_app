package ru.darenina.web.loans;

import ru.darenina.dao.LoanDAO;
import ru.darenina.model.Loan;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/loanslist")
public class LoansListServlet extends HttpServlet {

    private LoanDAO loanDAO;

    public void init() {
        loanDAO = new LoanDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        try {
            listLoan(request, response);

        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }

    private void listLoan(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        List<Loan> listLoan = loanDAO.selectAllLoans();
        request.setAttribute("listLoan", listLoan);
        RequestDispatcher dispatcher = request.getRequestDispatcher("loans-list.jsp");
        dispatcher.forward(request, response);
    }

}
