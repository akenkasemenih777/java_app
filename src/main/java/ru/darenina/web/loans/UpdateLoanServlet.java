package ru.darenina.web.loans;
import ru.darenina.dao.LoanDAO;
import ru.darenina.model.Loan;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/updateloan")
public class UpdateLoanServlet extends HttpServlet{

    private LoanDAO loanDAO;

    public void init() {
        loanDAO = new LoanDAO();
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {


        try {
            updateLoan(request, response);


        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }
    private void updateLoan(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ParseException {
        Date date_of_receipt = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("date_of_receipt"));
        Date last_payment_date = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("last_payment_date"));
        Date payment_term_date = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("payment_term_date"));
        float annual_interest = Float.parseFloat(request.getParameter("annual_interest"));
        float debt_balance = Float.parseFloat(request.getParameter("debt_balance"));
        float total_loan_amount = Float.parseFloat(request.getParameter("total_loan_amount"));
        int client_id = Integer.parseInt(request.getParameter("client_id"));
        int id = Integer.parseInt(request.getParameter("id"));
        Loan loan = new Loan(id,client_id,date_of_receipt, last_payment_date, payment_term_date, annual_interest,debt_balance, total_loan_amount);


        loanDAO.updateLoan(loan);
        response.sendRedirect("loanslist");
    }

}

