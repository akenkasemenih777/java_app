package ru.darenina.web.loans;

import ru.darenina.dao.LoanDAO;
import ru.darenina.model.Loan;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/deleteloan")
public class DeleteLoanServlet extends HttpServlet{

    private LoanDAO loanDAO;

    public void init() {
        loanDAO = new LoanDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        try {
            deleteLoan(request, response);


        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }
    private void deleteLoan(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        loanDAO.deleteLoan(id);
        response.sendRedirect("loanslist");

    }
}
