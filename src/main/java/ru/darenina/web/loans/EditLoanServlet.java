package ru.darenina.web.loans;

import ru.darenina.dao.LoanDAO;
import ru.darenina.model.Loan;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/editloan")
public class EditLoanServlet extends HttpServlet{

    private LoanDAO loanDAO;

    public void init() {
        loanDAO = new LoanDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        try {
            showEditFormLoan(request, response);


        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }

    private void showEditFormLoan(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Loan existingClient = loanDAO.selectLoan(id);
        RequestDispatcher dispatcher = request.getRequestDispatcher("loan-edit-form.jsp");
        request.setAttribute("loan", existingClient);
        dispatcher.forward(request, response);

    }

}

