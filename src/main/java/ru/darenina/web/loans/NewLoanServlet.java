package ru.darenina.web.loans;

import ru.darenina.dao.ClientDAO;
import ru.darenina.dao.LoanDAO;
import ru.darenina.model.Loan;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/newloan")
public class NewLoanServlet extends HttpServlet{

    private LoanDAO loanDAO;

    public void init() {
        loanDAO = new LoanDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {


        try {
            showNewFormLoan(request, response);


        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }
    private void showNewFormLoan(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("loan-form.jsp");
        dispatcher.forward(request, response);
    }
}
