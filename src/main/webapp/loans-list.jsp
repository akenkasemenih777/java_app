<%@ page import="ru.darenina.model.User" %>
<%@ page import="java.util.List" %>
<%@ page import="ru.darenina.model.Loan" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<html>
<head>
<title>User Management Application</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
	<div class="row">
		<div class="container" align="left">
			<p>

				<a href="<%=request.getContextPath()%>/list" class="btn btn-success">Сотрудники</a>
				<a href="<%=request.getContextPath()%>/clientslist" class="btn btn-success">Клиенты</a>
				<a href="<%=request.getContextPath()%>/loanslist" class="btn btn-success">Кредиты</a>
				<a href="<%=request.getContextPath()%>/debtlist" class="btn btn-success">Должники</a>
				<a href="<%=request.getContextPath()%>/auth" class="btn btn-success">Выход</a>
			</p>
		</div>

		<div class="container">
			<h3 class="text-center">Кредиты</h3>
			<hr>
			<div class="container text-left">

				<a href="<%=request.getContextPath()%>/newloan" class="btn btn-success">Добавить кредит</a>
			</div>
			<div class="container text-left">

			<br>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Дата</th>
						<th>Сумма</th>
						<th>Действия</th>
					</tr>
				</thead>
				<tbody>
				<%
					List<Loan> loan = (List<Loan>) request.getAttribute("listLoan");

					if (loan != null && !loan.isEmpty()) {
						for (Loan l : loan) {
							out.println("<tr>");
							out.println("<td>" + l.getDateOfReceipt() + "</td>");
							out.println("<td>" + l.getTotalLoanAmount() + "</td>");
							out.println("<td><a href=" + request.getContextPath() + "/editloan?id=" + l.getIdLoan() + ">Редактировать</a>");
							out.print("<a href=" + request.getContextPath() + "/deleteloan?id=" + l.getIdLoan() + ">Удалить</a></td>");
							out.println("</tr>");
						}
					} else out.println("<p>There are no users yet!</p>");
				%>
				</tbody>
			</table>
		</div>
	</div>
	</div>
</body>
</html>
